document.addEventListener('DOMContentLoaded', function() {
    const submitBtn = document.getElementById('submitBtn');

    submitBtn.addEventListener('click', function() {
        const xhr = new XMLHttpRequest();
        const name = document.getElementById('name').value;
        const gender = document.querySelector('input[name="gender"]:checked').value;
        const residence = document.getElementById('residence').value;

        const url = `process.php?name=${name}&gender=${gender}&residence=${residence}`;

        xhr.onreadystatechange = function() {
            if (xhr.readyState !== 4) {
                return;
            }
            if (xhr.status === 200) {
                const response = xhr.responseText;
                const cleanResponse = response.replace(/<[^>]+>/g, '');
                alert(cleanResponse);
            }
        };

        xhr.open('GET', url, true);
        xhr.send();
    });
});